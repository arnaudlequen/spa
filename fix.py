import random
import utils

# FIXING UTILS

# Return a vertex that doesn't have its Kirchoff's law satisfied
# Non-deterministic algorithm
def findBadVertex(E, k):
    n = len(E)

    ilist = list(range(n))
    random.shuffle(ilist)
    
    for i in ilist:
        if sum(E[i]) % k != 0:
            return i

    return -1


# Returns the vertices that are linked from and that are linked to i
def adjacentVertices(M, i):
    n = len(M)
    adj = ([], [])
    
    for j in range(n):
        if M[i][j] == 1:
            adj[0].append(j)
        elif M[i][j] == -1:
            adj[1].append(j)
        
    return adj
            
# SIMPLE FIX

# Finds another assignation of an edge of i such that Kirchoff's law is satisfied
def fixVertex(M, E, i, k):
    #print(E)
    d = sum(E[i]) % k
    
    if d == 0:
        return "Nothing to fix"
    
    adjOut, adjIn = adjacentVertices(M, i)
    adj = adjIn.copy()
    adj.extend(adjOut)

    j = -1
    
    # We are sometimes putting valuations of edges to zero
    diffIn = [int(sum(E[j])) % k for j in adjIn]
    if d in diffIn:
        j = diffIn.index(d)
        E[i][adjIn[j]] += d
        E[adjIn[j]][i] -= d
                
        return "Success"
    
    diffOut = [int(sum(E[j])) % k for j in adjOut]
    if ((-d)%k) in diffOut:
        j = diffOut.index((-d)%k)
        E[i][adjOut[j]] -= d
        E[adjOut[j]][i] += d
        
        return "Success"

    return "Failed"

# MULTIPLE FIX

# Change the valuation of multiple edges coming from and exiting vertex i
def multipleFixVertex(M, E, i, k):
        #print(E)
    d = sum(E[i]) % k
    
    if d == 0:
        return "Nothing to fix"
    
    adjOut, adjIn = adjacentVertices(M, i)
    adj = adjIn.copy()
    adj.extend(adjOut)
    
    # We are sometimes putting valuations of edges to zero
    diffIn  = [int(sum(E[j])) % k for j in adjIn]
    diffOut = [-1*(int(sum(E[j])) % k) for j in adjOut]

    diff = diffIn
    diff.extend(diffOut)

    values = [[] for _ in range(k)]
    nbrNbrg = len(adj)

    # To compute every way every value can be attained, we add a new edge iteratively
    for l in range(nbrNbrg):
        oldvalues = utils.deepCopy3(values)

        for v in range(1, k):
            # vList : list of edges (given by the neighbouring vertex) such that the sum adds up to v, for vertex i
            newval = (v + diff[l]) % k

            for vList in oldvalues[v]:
                newvList = vList.copy()
                newvList.append(l)
                values[newval].append(newvList)

        values[diff[l]].append([l])

    # The whole list is generated so that we can make non-deterministic choices, at the cost of performance
    if len(values[d]) == 0:
        return "Failed"

    verticesToChange = values[d][random.randint(0, len(values[d])-1)]

    for neigh in verticesToChange:
        j = adj[neigh]
        E[i][j] -= diff[neigh]
        E[j][i] += diff[neigh]
                
    return "Success"


# ZERO FIXING

# Find an edge whose valuation is zero
# Non-deterministic so that we can attempt to fix as many zeros as possible,
# and not stumble on the first unsuccessful fix attempt
def findZeroEdge(E, M, k):
    n = len(E)

    li = list(range(n))
    random.shuffle(li)
    lj = list(range(n))
    random.shuffle(lj)
    for i in li:
        for j in lj:
            if E[i][j] == 0 % k and M[i][j] != 0:
                return (i, j)
    
    return (-1, -1)

# Find a non-trivial path from s0 to s1 such that it is possible to add v to the
# valuation of each vertex without putting it to zero
# s0 and s1 have to be linked by an edge
# Use of a BFS to make sure we find a cycle as soon as possible
def findCycle(s0, s1, E, M, v, k):
    # If there is no edge between s0 and s1
    if M[s0][s1] == 0:
        return [-1]

    n = len(E)
    queue = [s0]

    predecessors = [-1]*n
    marked = [False]*n

    while len(queue) > 0:
        current = queue.pop(0)
        marked[current] = True

        if current == s1:
            path = [s1]
            
            # Should be fine
            while path[-1] != s0:
                path.append(predecessors[path[-1]])

            path.reverse()
            return path

        # Randomly choose the next edge so that the algorithm non-deterministically returns a cycle
        l = list(range(n))
        random.shuffle(l)
        for i in l:
            if ((M[current][i] == 1 and (E[current][i] + v) % k != 0) or (M[current][i] == -1 and (E[i][current] - v) % k != 0)) and (current != s0 or i != s1) and not marked[i]:
                queue.append(i)
                predecessors[i] = current

    return [-1]


# Find a cycle containing the edge with a zero
# Add a number to each edge of that cycle to remove the zero
# Returns True if it succeeded
def fixZero(s0, s1, E, M, k):
    for v in range(1, k):
        cycle = findCycle(s0, s1, E, M, v, k)
        if cycle != [-1]:
            cycle.append(cycle[0])

            for ic in range(len(cycle) - 1):
                i = cycle[ic]
                j = cycle[ic+1]
                
                if M[i][j] == 1:
                    E[i][j] = (E[i][j] + v) % k
                    E[j][i] = -1*E[i][j]
                else:
                    E[j][i] = (E[j][i] - v) % k
                    E[i][j] = -1*E[j][i]
                #E[i][j] = (E[i][j] + M[i][j]*(k-v)) % k
                #E[j][i] = -1*E[i][j]

            print("Fixed zero " + str(s0) + " " + str(s1) + " in cycle ", end="")
            print(cycle[:-1], end="")
            print(" by adding " + str(v))
            return True

    return False


# PATH-FINDING TECHNIC

# DFS to find a path from vertex s0 to vertex s1 so that we can substract
# value v on an edge without putting it to zero. Non deterministic 
# algorithm so that a random path is returned every time
def findPath(s0, s1, E, M, v, k):
    n = len(E)
    stack = [s0]

    predecessors = [-1]*n
    marked = [False]*n

    while len(stack) > 0:
        current = stack.pop()
        marked[current] = True

        if current == s1:
            path = [s1]
            
            while path[-1] != s0:
                path.append(predecessors[path[-1]])

            path.reverse()
            return path

        # Randomly choose the next edge so that the algorithm non-deterministically returns a cycle
        l = list(range(n))
        random.shuffle(l)
        for i in l:
            if ((M[current][i] == 1 and (E[current][i] - v) % k != 0) or (M[current][i] == -1 and (E[i][current] + v) % k != 0)) and (current != s0 or i != s1) and not marked[i]:
                stack.append(i)
                predecessors[i] = current

    return [-1]


# Find a path linking two vertices whose Kirchoff's law is not satisfied
# and add what's needed along the path, fixing those vertices
def pathFixVertex(M, E, s0, k):
    n = len(M)
    s = sum(E[s0])
    d = utils.sgn(s)*(s % k)
    
    if d == 0:
        return "Nothing to fix"
    
    # Check other vertices non-deterministically
    otherList = list(range(n))
    otherList.remove(s0)
    random.shuffle(otherList)

    s1 = -1

    for j in otherList: 
        s = sum(E[j])
        s = -1*utils.sgn(s) * (s % k)

        # Always try to find the best match
        if s == d:
            s1 = j
            break

        # We can settle for any other bad vertex else
        if s != 0:
            s1 = j

    if s1 == -1:
        return "Failed to find a similar vertex"

    # Trying to substract d on every edge
    path = findPath(s0, s1, E, M, d, k)

    if path == [-1]:
        return False

    for ip in range(len(path) - 1):
        i = path[ip]
        j = path[ip+1]
        
        if M[i][j] == 1:
            E[i][j] = (E[i][j] - d) % k
            E[j][i] = -1*E[i][j]
        else:
            E[j][i] = (E[j][i] + d) % k
            E[i][j] = -1*E[j][i]


    print("Fixed vertices " + str(s0) + " and " + str(s1) + " in path ", end="")
    print(path, end="")
    print(" by substracting " + str(d))

    return True
