# UTILS
# Matrices
def matsum(A, B):
    n = len(A)
    
    C = [[0]*n for _ in range(n)]
    
    for i in range(n):
        for j in range(n):
            C[i][j] = A[i][j] + B[i][j]
            
    return C
    
def matscal(l, A):
    n = len(A)
    C = [[0]*n for _ in range(n)]
    
    for i in range(n):
        for j in range(n):
            C[i][j] = A[i][j]*l
            
    return C
    
# Distance between two matrices
def dist(A, B):
    n = len(A)
    
    s = 0
    for i in range(n):
        for j in range(n):
          s += abs(A[i][j] - B[i][j])
          
    return s

# Prints a matrix in the terminal
def printMatrix(M):
    for l in M:
        print(l)


# Sets
#Intersection of two sets
def intersect(A, B):            
    return [e for e in A if e in B]

# Lists
# Deep copy of a list of lists of lists
def deepCopy3(l):
    lc = []

    for i in range(len(l)):
        lc0 = []
        for j in range(len(l[i])):
            lc0.append(l[i][j])
        lc.append(lc0)

    return lc
    
# Arithmetic
# Sign of a number
def sgn(n):
    return 1 if n >= 0 else -1

