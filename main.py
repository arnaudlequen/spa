# Require apt-get install python3-graphviz and graphviz
from graphviz import Digraph
import random

# Custom libraries
import utils
import graphs
import dmap
import error    
import fix


def main():
    #Number of vertices and edges
    n, m = list(map(int, input().split()))

    #Beta of the Difference map algorithm
    BETA = -0.5
    
    MAXITER  = 200
    MAXFIXES = 3 * m
    MAXMULTFIXES = 5 * m
    MAXPATHFIXES = 3 * m
    MAXZEROTRIES = 2 * m
    MAXZEROFAILS = m
    
    #We will be working in Z/kZ
    k = int(input())
    
    #Adjacency matrix
    M = [[0]*n for _ in range(n)]

    # Set to True as soon as a valuation is found
    foundValuation = False
    
    for v in range(m):
        i, j = list(map(int, input().split()))
        
        if M[j][i] == 0:
            M[i][j] = 1
        else:
            print("Error: cannot add an edge whose antisymetric edge is already in the graph")

    
    # Add edges that exit vertices
    for i in range(n):
        for j in range(n):
            M[j][i] = -1 if M[i][j] == 1 else M[j][i]

    E = [[0] * n for _ in range(n)]    


    # CHECK IF THE GRAPH IS BRIDGELESS
    if not graphs.isBridgeless(M):
        print("No valuation exist")
        return
    
    # FIND AN INITIAL VALUATION
    # Difference Map Algorithm
    if MAXITER > 0:
        # Arbitrary first valuation
        E = [M[i].copy() for i in range(n)]

        S = dmap.diffmap(BETA, MAXITER, E, k)
        E = [list(map(int, S[i])) for i in range(n)]
        
        if error.error(E, k) == 0:
            print("Valuation is correct, stopping")
            foundValuation = True
        

    # REFINE THE VALUATION
    # Quick fixes : attempts to change individual edges
    if not foundValuation:
        print("Fixing edges...")

        i = 0
        while (i < MAXFIXES) and (error.error(E, k) > 0):
            vertex = fix.findBadVertex(E, k)
            print("Fixing vertex " + str(vertex) + "...", end="")
            
            # Side effect : fixVertex changes the value of the assignation E
            tryfix = fix.fixVertex(M, E, vertex, k) 
            print(tryfix, end="")
            print(" - Error=" + str(error.error(E, k)))

            i += 1
        
        if error.error(E, k) == 0:
            foundValuation = True

    # Longer fixes : attempts to change sets of edges relative to a vertex
    if not foundValuation:
        print("Fixing multiple edges...")

        i = 0
        while (i < MAXMULTFIXES) and (error.error(E, k) > 0):
            vertex = fix.findBadVertex(E, k)
            print("Fixing vertex " + str(vertex) + "...", end="")

            tryfix = fix.multipleFixVertex(M, E, vertex, k)
            print(tryfix, end="")
            print(" - Error=" + str(error.error(E, k)))

            i += 1
        
        if error.error(E, k) == 0:
            foundValuation = True

    # Even longer fixes : attepts to fix vertices that are far apart
    #print(findPath(0, 4, E, M, 2, k))
    if not foundValuation:
        print("Fixing pairs of vertices linked by a path...")

        i = 0
        while (i < MAXPATHFIXES) and (error.error(E, k) > 0):
            vertex = fix.findBadVertex(E, k)
            print("Fixing vertex " + str(vertex))

            tryfix = fix.pathFixVertex(M, E, vertex, k)
            print(tryfix, end="")
            print(" - Error=" + str(error.error(E, k)))

            i += 1 

        if error.error(E, k) == 0:
            foundValuation = True


    # REMOVE LAST ZEROS
    # Find cycles zeros are in, to remove them
    error.normalizeEdges(E, M, k)

    i = 0
    failsInARow = 0
    while (i < MAXZEROTRIES) and error.existZeros(E, M, k) and (failsInARow < MAXZEROFAILS):
        print("Fix attempt " + str(i))
        s0, s1 = fix.findZeroEdge(E, M, k)
        success = fix.fixZero(s0, s1, E, M, k)
        if not success:
            failsInARow += 1
        i += 1


    # CORRECTNESS TEST
    if error.error(E, k) == 0 and not error.existZeros(E, M, k):
        print("Found a correct valuation!")
    else:
        print("Last found valuation is incorrect")
        print("Error : " + str(error.error(E, k)))
        print("Remaining zeros : " + str(error.countZeros(E, M, k)))


    # GRAPH DRAWING
    dotgraph = Digraph("Graph in Z/" + str(k) + "Z", filename="output.gv")

    dotgraph.attr('node', shape="circle")
    for i in range(n):
        #dotgraph.node("N" + str(i), label="")
        dotgraph.node("N" + str(i))
    
    dotgraph.attr('edge', color="#555555")
    for i in range(n):
        for j in range(n):
            if M[i][j] == 1:
                dotgraph.edge("N" + str(i), "N" + str(j), label=str(E[i][j] % k))

    dotgraph.view()


main()
