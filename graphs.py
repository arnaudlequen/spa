# GRAPH ALGORITHMS

# Computes the number of connex components of a graph given by its adjacency matrix
# Digraphs are seen as non-oriented graphs
# Basically a DFS
def connexComponents(G):
    n = len(G)
    components = [0]*n

    nbrComponents = 0

    while 0 in components:
        root = components.index(0)
        stack = [root]
        nbrComponents += 1
        components[root] = nbrComponents

        while len(stack) > 0:
            i = stack.pop()
            
            for j in range(n):
                if abs(G[i][j]) == 1 and components[j] == 0:
                    stack.append(j)
                    components[j] = nbrComponents

    return nbrComponents

# Returns True if a graph G is bridgeless
# Complexity : O(nm)
def isBridgeless(G):
    n = len(G)

    initialComponents = connexComponents(G)

    for i in range(n):
        for j in range(i, n):
            if G[i][j] == 0:
                continue

            # Graph with a deleted edge
            Gs = [G[i].copy() for i in range(n)]
            Gs[i][j] = 0
            Gs[j][i] = 0
            if connexComponents(Gs) > initialComponents:
                print("Edge (" + str(i) + ", " + str(j) + ") is a bridge")
                return False

    return True
