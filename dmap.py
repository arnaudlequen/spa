import utils

#Difference map algorithm
def projA(A, E, k):
    C = [E[i].copy() for i in range(len(E))]
    m = len(A)//2
    
    for i in range(m):
        a, b = A[2*i], A[2*i+1]
        
        
        c = abs(round((E[a][b] - E[b][a])/2) % k)
        if c == 0 % k:
            if E[a][b] + E[b][a] > 0:
                c += 1
            else:
                c += -1
            
        C[a][b] = c
        C[b][a] = -c
    
    return C


def projB(B, E, k):
    C = [E[i].copy() for i in range(len(E))]
    n = len(B)
    
    for i in range(n):        
        if len(B[i]) == 0:
            continue
        s = sum([E[i][j] for j in B[i]]) / len(B[i]) 
        
        for j in B[i]:
            C[i][j] -= s
            #C[i][j] = C[i][j] % k
            
    return C
    
    
# Iteration of the difference map algorithm
def step(E, A, B, b, k):
    Ec = [E[i].copy() for i in range(len(E))]
    PA0 = projA(A, E, k)
    PA1 = utils.matsum(utils.matscal(1-1/b, PA0), utils.matscal(1/b, E))

    PB0 = projB(B, E, k)
    PB1 = utils.matsum(utils.matscal(1+1/b, PB0), utils.matscal(-1/b, E))
    
    PA2 = projA(A, PB1, k)
    PB2 = projB(B, PA1, k)
    
    D = utils.matsum(Ec, utils.matscal(b, utils.matsum(PA2, utils.matscal(-1, PB2))))
    
    D2 = projA(A, E, k)
    
    return D
    
# Difference map algorithm
def diffmap(BETA, MAXITER, Ec, k):
    n = len(Ec)
    E = [Ec[i].copy() for i in range(len(Ec))]
    
    B = [[] for _ in range(n)]
    A = []
    
    for i in range(n):
        for j in range(n):
            if E[i][j] == 1:
                B[i].append(j)
                B[j].append(i)
                
                A.extend([i, j])

    S = step(E, A, B, BETA, k)
    i = 1
    m = float("inf")
    
    while((i < MAXITER) and (not S == E)):
        print("Iteration " + str(i) + "...")
        E = [S[i].copy() for i in range(len(S))]
        S = step(E, A, B, BETA, k)
        m = min(utils.dist(S, E), m)
        
        i += 1
    
    if i == MAXITER:
        print("No value found")
        print("Last value : ")
        print(projA(A, E, k))
        return projA(A, E, k)
        
    print("Min value : " + str(m))
    print("Value found in " + str(i) + " iterations")
    print(projA(A, E, k))
    return projA(A, E, k)
