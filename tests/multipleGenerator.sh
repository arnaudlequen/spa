for n in `seq 5 10 150`; do
    for m in `seq $((2*$n)) $(($n*($n-1)/12)) $(($n*($n-1)/2))`; do
        let "i = $i+1"
        printf -v n "%02d" $i;
        touch ./auto/Z5Z/test$i
        python3 generator.py $n $m 5 > ./auto/Z5Z/test$i
    done;
done;
