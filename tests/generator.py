import argparse
import random
from itertools import product


parser = argparse.ArgumentParser(description='Generate a random problem, with a random graph with n vertices and m edges.')
parser.add_argument('n', metavar='vertices', type=int,
                   help='number of vertices of the graph')
parser.add_argument('m', metavar='edges', type=int,
                   help='number of edges of the graph')
parser.add_argument('k', metavar='grouporder', type=int,
                   help='group of the form Z/kZ in which the vertices will find their valuations')
parser.add_argument('bridgeless', metavar='bridgeless', type=int,
                   help='1 or 0 : has the graph to be bridgeless?')

args = parser.parse_args()

# Count the degree
def degree(M, i):
    n = len(M)
    count = 0

    for j in range(n):
        count += abs(M[i][j])

    return count

def zeros(M, i):
    n = len(M)
    z = []

    for j in range(n):
        if abs(M[i][j]) == 1:
            z.append(j)

    return z


def main():
    n = args.n
    m = args.m
    k = args.k

    M = [[0]*n for _ in range(n)]

    if m > n*(n-1) // 2:
        print("No such graph : too many vertices")
        return

    print(str(n) + " " + str(m))
    print(str(k))

    edges = [(i, j) for i, j in product(range(n), range(n)) if i != j]
    
    for _ in range(m):
        a = random.randint(0, len(edges) - 1)
        i, j = edges[a]
        edges.remove((i, j))
        edges.remove((j, i))

        M[i][j] = 1
        M[j][i] = -1

        print(str(i) + " " + str(j))

    if args.bridgeless == 1:
        for i in range(n):
            if degree(M, i) < 3:
                z = zeros(M, i)
                j = z[random.randint(0, len(z) - 1)]
                sgn = 1 - 2*(random.randint(0, 1))
                
                M[i][j] = sgn
                M[j][i] = -sgn

                if sgn > 0:
                    print(str(i) + " " + str(j))
                else:
                    print(str(j) + " " + str(i))

    
main()
    

    