# ERROR CONTROL

# Sum of the minimal values that have to be added to edges of the graph so
# that the valuation is correct
def error(E, k):
    n = len(E)
    
    s = 0
    for i in range(n):
        s += abs(sum(E[i]) % k)
    
    return s


# Set of all vertices that don't satisfy Kirchoff's laws
def wrongVertices(E, k):
    n = len(E)
    
    w = []
    for i in range(n):
        if (sum(E[i]) % k) != 0:
            w.append(i)
            
    return w

# Return the number of edges valued to zero
def countZeros(E, M, k):
    n = len(E)
    c = 0

    for i in range(n):
        for j in range(n):
            if E[i][j] == 0 % k and M[i][j] == 1:
                c += 1
    
    return c

# Return True if there are edges whose valuation is zero
def existZeros(E, M, k):
    n = len(E)

    for i in range(n):
        for j in range(n):
            if E[i][j] == 0 % k and M[i][j] != 0:
                return True
    
    return False

# Makes sure that if (i, j) exists, E[i][j] is positive
def normalizeEdges(E, M, k):
    n = len(E)

    for i in range(n):
        for j in range(n):
            if M[i][j] == 1:
                E[i][j] = (E[i][j] + k) % k
                E[j][i] = -1*E[i][j]
